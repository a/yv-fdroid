## yv-fdroid

Unofficial F-Droid repo for YouTube Vanced, because it's a pain to keep it up to date otherwise.

It takes the build from the top of the table on YouTube Vanced site, which is White/Black, which is what I prefer. If you want White/Dark, you'll have to mess with the code a bit.

---

#### Repo URLs

- Non-Root: https://fdroid.a3.pm/yvnr/repo
- Root: https://fdroid.a3.pm/yvr/repo

---

#### Using the code here

If you want to host this yourself, you will need to install `fdroidserver` and a Python version over 3.6.

Also, you'll need to create a repo by going to a folder (such as `/var/www/fdroid/yvnr`), running `fdroid init` and copying metadata folder there.

After that, all you need to do is run this script periodically as `python3 getgv.py $repo_folder` (do not include spaces on repo folder, don't use the `/repo` path, use the base repo path).

If you want the root variant, run as `python3 getgv.py $repo_folder root`.

