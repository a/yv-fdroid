from bs4 import BeautifulSoup
import requests
import os
import sys
import subprocess

fdroid_folder = sys.argv[1]
result_folder = fdroid_folder + "/repo"
root = ("root" in sys.argv)


def download_file(url, filename):
    # Based on http://masnun.com/2016/09/18/
    # python-using-the-requests-module-to-download-large-files-efficiently.html
    response = requests.get(url, stream=True)
    if response.status_code != 200:
        raise Exception(f"Response code for {url} is {response.status_code}")
    handle = open(filename, "wb")
    for chunk in response.iter_content(chunk_size=512):
        if chunk:  # filter out keep-alive new chunks
            handle.write(chunk)


def get_yv_urls(root=False):
    url = f"https://vanced.app/APKs?type={'' if root else 'NON'}"\
          "ROOT&style=BASIC"
    html = requests.get(url)

    urls = {}

    soup = BeautifulSoup(html.text, 'html.parser')

    tables = soup.find_all("table")

    # YouTube Vanced
    yv_table = tables[0] if root else tables[1]
    url = yv_table.tbody.tr.a.get("href")
    apkid = url.split("fileId=")[-1]
    urls[f"yv-{apkid}.apk"] = "https://vanced.app" + url

    # microG
    if not root:
        mg_table = tables[0]
        url = mg_table.tbody.find_all("tr")[-1].a.get("href")
        apkid = url.split("fileId=")[-1]
        urls[f"mg-{apkid}.apk"] = "https://vanced.app" + url

    return urls


fdroid_update_needed = False

yv_urls = get_yv_urls(root)

# Go through URLs and get the non-existant ones
for fname in yv_urls:
    url = yv_urls[fname]
    file_path = os.path.join(result_folder, fname)
    if not os.path.exists(file_path):
        fdroid_update_needed = True
        download_file(url, file_path)

# Do fdroid magic if we downloaded anything
if fdroid_update_needed:
    subprocess.run(f"fdroid update -c",
                   shell=True,
                   cwd=fdroid_folder)
